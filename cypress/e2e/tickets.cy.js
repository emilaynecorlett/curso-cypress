describe("Ticketes", () => {
    beforeEach(() => cy.visit("https://bit.ly/2XSuwCW"));    
    it('fills all the text input fields', () => {
        const firstName = "Emilayne";
        const lastName = "Corlett";

        cy.get('#first-name').type(firstName);
        cy.get('#last-name').type(lastName);
        cy.get('#email').type("emilaynecorlett@gmail.com");
        cy.get('#requests').type("Vegetarian");
        cy.get('#signature').type(`${firstName} ${lastName}`);
    });
    it('select two tickets', () => {
        cy.get('#ticket-quantity').select('2');
    });

    
    it('Select "vip" ticket type', () => {
        cy.get('#vip').check();        
    });

    it('Selects "social media" checkbox', () => {
        cy.get('#social-media').check();        
    });

    it('Selects "friend" and "publication", then uncheck "friend"', () => {
        cy.get('#friend').check();
        cy.get('#social-media').check();
        cy.get('#friend').uncheck();             
    });
    it("has 'TICKETBOX' header's heading", () => {
        cy.get('header h1').should('contain', 'TICKETBOX');
    });

    it('alerts on invalid email', () => {
        cy.get('#email')
        .as('email')
        .type('emilayne.corlett-gmail.com');

        cy.get('#email.invalid')
        .as('invalidEmail')
        .should('exist');

        cy.get('@email')
        .clear()
        .type('emilayne.corlett@gmail.com');

        cy.get('@invalidEmail').should('not.exist');
    });

    it('fills and resets the form', () => {
        const firstName = "Emilayne";
        const lastName = "Corlett";
        const fullName = `${firstName} ${lastName}`;

        cy.get('#first-name').type(firstName);
        cy.get('#last-name').type(lastName);
        cy.get('#email').type("emilaynecorlett@gmail.com");
        cy.get('#ticket-quantity').select('2');
        cy.get('#vip').check();
        cy.get('#friend').check();
        cy.get('#requests').type("Gyn");
        cy.get('#signature').type(`${firstName} ${lastName}`);

        cy.get('.agreement p')
        .should("contain",`I, ${fullName}, wish to buy 2 VIP tickets.`);

        cy.get('#agree').click();
        cy.get('#signature').type(fullName);

        cy.get('button[type="submit"]')
        .as('submitButton')
        .should('not.be.disabled');

        cy.get('button[type="reset"]').click();

        cy.get('@submitButton').should('be.disabled');
        
    });

    it('fills mandatory fields using support command', () => {
        const customer = {
            firstName: 'Ana',
            lastName: 'Rodrigues',
            email: 'anarodriguest@gmail.com'
        };
  
        cy.fillMandatoryFields(customer);

        cy.get('button[type="submit"]')
        .as('submitButton')
        .should('not.be.disabled');

        cy.get('#agree').uncheck();

        cy.get('@submitButton').should('be.disabled');
    });
    

});